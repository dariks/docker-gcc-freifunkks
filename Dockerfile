FROM gcc

MAINTAINER Daniel Rippen <rippendaniel@gmail.com>

ENV DEBIAN_FRONTEND noninteractive

RUN	apt-get update && \
        apt-get install -y apt-utils && \
        apt-get upgrade -y && \
	apt-get install -y gawk unzip genisoimage && \
	apt-get clean && \
	rm -rf /var/lib/apt/lists/*
